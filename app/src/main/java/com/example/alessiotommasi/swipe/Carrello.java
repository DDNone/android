package com.example.alessiotommasi.swipe;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

public class Carrello extends AppCompatActivity {

    ImageView imgLogin;
    TextView txtUserName,txtNotLogged;
    ListView scrlCarrello;
    Button btnOrdina;
    String isLogged;
    public Context context=this;
    //list
    public static ArrayList<DataModel> dataModels=new ArrayList<>();
    ListView listView;
    public static CustomAdapter adapter;
    static String id;
    //InternalStorageUtil isu= new InternalStorageUtil();
    //ArrayList <DataModel> paniniCarrello= new ArrayList<DataModel>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_carrello);

        btnOrdina=(Button)findViewById(R.id.btnOrdina);
        imgLogin= (ImageView) findViewById(R.id.imgLoginCarrello);
        txtUserName=(TextView) findViewById(R.id.txtUserLog);
        scrlCarrello=(ListView) findViewById(R.id.list);
        txtNotLogged=(TextView) findViewById(R.id.txtNotLogged);


        //is logged true è loggato, false/catch non lo è;
        try {
            isLogged=getIntent().getExtras().getString("isLogged");
            //mTextMessage.setText(isLogged);

            if(isLogged.equals("true")){
                imgLogin.setVisibility(View.GONE);
                txtUserName.setVisibility(View.VISIBLE);
                scrlCarrello.setVisibility(View.VISIBLE);
                txtNotLogged.setVisibility(View.GONE);
                String User=getIntent().getExtras().getString("UserName");
                id=getIntent().getExtras().getString("id");

                txtUserName.setText(id+ " Benvenuto "+User);
            }
            else {
                imgLogin.setVisibility(View.VISIBLE);
                txtUserName.setVisibility(View.GONE);
                scrlCarrello.setVisibility(View.GONE);
                txtNotLogged.setVisibility(View.VISIBLE);
                this.imgLogin=(ImageView)findViewById(R.id.imgLogin);


            }
        }catch (Exception e){
            imgLogin.setVisibility(View.VISIBLE);
            txtUserName.setVisibility(View.GONE);
            scrlCarrello.setVisibility(View.GONE);
            txtNotLogged.setVisibility(View.VISIBLE);
        }



        try {
            //non ho bisogno di leggere su file finche il programma è in esecuzione
            //dataModels=(ArrayList <DataModel>)isu.readObject(this,"prova");

        } catch (Exception e) {
            e.printStackTrace();
        }

        //list part
        try {
            listView = (ListView) findViewById(R.id.list);
            //dataModels=paniniCarrello;
            adapter = new CustomAdapter(dataModels, getApplicationContext());
            listView.setAdapter(adapter);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    DataModel dataModel = dataModels.get(position);

                    //code So
                    AlertDialog.Builder adb=new AlertDialog.Builder(Carrello.this);
                    adb.setTitle("Delete?");
                    adb.setMessage("Are you sure you want to delete " + dataModel.getName()+"?");
                    final int positionToRemove = position;
                    adb.setNegativeButton("Cancel", null);
                    adb.setPositiveButton("Ok", new AlertDialog.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dataModels.remove(positionToRemove);
                            adapter.notifyDataSetChanged();

                        }});
                    adb.show();

                    //end
                    Snackbar.make(view,"debag...", Snackbar.LENGTH_LONG)
                            .setAction("No action", null).show();
                }
            });
        }catch (Exception e){e.printStackTrace();
        }
        //endlist

        //ordina part
        btnOrdina.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //dbpart
                new Ordina(getApplicationContext()).execute();

                //messaggio
                AlertDialog.Builder adb=new AlertDialog.Builder(Carrello.this);
                adb.setTitle("Ordine");
                adb.setMessage("ordine effettuato con successo! ");
                adb.setNegativeButton("Cancel", null);
                adb.setPositiveButton("Ok", new AlertDialog.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dataModels.clear();
                        adapter.notifyDataSetChanged();
                    }});
                adb.show();

            }
        });

        imgLogin.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //dbpart
                Intent myIntent = new Intent(getApplicationContext(),LoginActivity.class);
                startActivity(myIntent);
            }
        });


    }


}
