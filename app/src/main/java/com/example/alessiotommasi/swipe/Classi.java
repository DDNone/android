package com.example.alessiotommasi.swipe;

public class Classi {
    String idClasse,mail,pw;

    public Classi(String idClasse, String mail, String pw) {
        this.idClasse = idClasse;
        this.mail = mail;
        this.pw = pw;
    }

    public String getIdClasse() {
        return idClasse;
    }

    public void setIdClasse(String idClasse) {
        this.idClasse = idClasse;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPw() {
        return pw;
    }

    public void setPw(String pw) {
        this.pw = pw;
    }
}
