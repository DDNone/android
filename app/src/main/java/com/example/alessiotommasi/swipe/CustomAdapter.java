package com.example.alessiotommasi.swipe;

import android.content.Context;
import android.content.DialogInterface;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.util.ArrayList;

public class CustomAdapter extends ArrayAdapter<DataModel> implements View.OnClickListener{

    private ArrayList<DataModel> dataSet;
    Context mContext;
    InternalStorageUtil isu= new InternalStorageUtil();
    // View lookup cache
    private static class ViewHolder {
        TextView txtName;
        TextView txtPrezzo;

        ImageView info;
    }



    public CustomAdapter(ArrayList<DataModel> data, Context context) {
        super(context, R.layout.row_item, data);
        this.dataSet = data;
        this.mContext=context;

    }


    @Override
    public void onClick(View v) {
        int position=(Integer) v.getTag();
        Object object= getItem(position);
        DataModel dataModel=(DataModel)object;




        switch (v.getId())
        {

            case R.id.item_info:

                AlertDialog.Builder adb=new AlertDialog.Builder(mContext);
                adb.setTitle("Delete?");
                adb.setMessage("Are you sure you want to delete " + dataModel.getName()+"?");
                final int positionToRemove = position;
                Carrello.dataModels.remove(positionToRemove);
                Carrello.adapter.notifyDataSetChanged();


                try {
                    isu.writeObject(getContext(), "prova", Carrello.dataModels);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                break;


        }


    }

    private int lastPosition = -1;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        DataModel dataModel = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder; // view lookup cache stored in tag

        final View result;

        if (convertView == null) {


            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.row_item, parent, false);
            viewHolder.txtName = (TextView) convertView.findViewById(R.id.txtName);
            viewHolder.txtPrezzo = (TextView) convertView.findViewById(R.id.txtPrezzoC);
            viewHolder.info = (ImageView) convertView.findViewById(R.id.item_info);

            result=convertView;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result=convertView;
        }


        lastPosition = position;


        viewHolder.txtName.setText(dataModel.getName());
        viewHolder.txtPrezzo.setText(dataModel.getPrezzo());

        viewHolder.info.setOnClickListener(this);
        viewHolder.info.setTag(position);
        // Return the completed view to render on screen
        return convertView;
    }


}

