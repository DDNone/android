package com.example.alessiotommasi.swipe;

import java.io.Serializable;

public class DataModel implements Serializable {

    String name;
    String prezzo;


    public DataModel(String name, String prezzo) {
        this.name = name;
        this.prezzo = prezzo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrezzo() {
        return prezzo;
    }

    public void setPrezzo(String prezzo) {
        this.prezzo = prezzo;
    }
}
