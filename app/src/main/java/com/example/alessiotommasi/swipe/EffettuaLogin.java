package com.example.alessiotommasi.swipe;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;

public class EffettuaLogin extends AsyncTask {

    public String str;
    Context context;
    public JSONArray objresponse;
    ArrayList<Classi> cls = new ArrayList();
    EditText txtUser, txtPw;

    public EffettuaLogin(Context context, EditText txtUser, EditText txtPw) {
        try {
            String var= "EffettuaLogin(Context context)";
            this.context = context;
            this.txtUser=txtUser;
            this.txtPw= txtPw;
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    @Override
    protected JSONArray doInBackground(Object[] objects) {
        String var= "doInBackground";
        try {
            loadUser();

        }catch (Exception e){
            e.printStackTrace();
        }

        return objresponse;
    }

    public void loadUser(){// funzia controllare context
        // Read Server Response indirizzo ip
        StringRequest stringRequest = new StringRequest(Request.Method.GET, "http://panelpanini.altervista.org/Login.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            //converting the string to json array object
                            objresponse = new JSONArray(response);
                            Control();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                });

        //adding our stringrequest to queue
        Volley.newRequestQueue(context).add(stringRequest);


        //end db
    }

    public void Control(){//controllo se ok parametri
        try {

            String id, mail, pw;
            JSONObject jAp;
            Classi ap=null;


            int i;
            for ( i= 0; i < this.objresponse.length(); i++) {
                jAp = this.objresponse.getJSONObject(i);
                id = jAp.getString("idClasse");
                mail = jAp.getString("mail");
                pw = jAp.getString("pw");

                ap = new Classi(id, mail, pw);
                //cls.add(ap);

                if(controlloPassword(mail,pw)){
                    break;
                }
            }


            //controllo pwd
            String ris = "false";//benvenuto username

            if(i<this.objresponse.length()){//pw e user corretto
                Intent myIntent = new Intent(context, MainActivity.class);
                ris="true";
                myIntent.putExtra("user", this.txtUser.getText().toString());
                myIntent.putExtra("id", ""+ap.getIdClasse());
                myIntent.putExtra("isLogged", ris);
                context.startActivity(myIntent);
            }
            else{//pw o user non corretto
                //non funziona
                Toast.makeText(context,"error",Toast.LENGTH_LONG);
                Intent myIntent = new Intent(context, LoginActivity.class);

                context.startActivity(myIntent);
            }

        }catch(Exception e){
            e.printStackTrace();
        }
    }


    public boolean controlloPassword(String mail, String pw){
        // QUERY Su DB per controllare
        String str=txtUser.getText().toString();

        if(this.txtUser.getText().toString().equals(mail)){
            if(this.txtPw.getText().toString().equals(pw))
                return true;
        }

        return false;
    }

    protected void onPreExecute(){
    }
}
