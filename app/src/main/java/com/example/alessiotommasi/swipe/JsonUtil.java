package com.example.alessiotommasi.swipe;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class JsonUtil {

    public static String toJSon(ArrayList<DataModel> dataModels) {
        try {
            // Here we convert Java Object to JSON
            JSONObject jsonObj = new JSONObject();

            // In this case we need a json array to hold the java list
            JSONArray jsonArr = new JSONArray();

            for (DataModel ap : dataModels) {
                JSONObject pnObj = new JSONObject();
                pnObj.put("name", ap.name);
                pnObj.put("prezzo", ap.prezzo);
                jsonArr.put(pnObj);
            }


            jsonObj.put("ordine", jsonArr);

            return jsonObj.toString();

        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        return null;

    }
}