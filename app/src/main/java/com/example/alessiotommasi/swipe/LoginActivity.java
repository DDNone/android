package com.example.alessiotommasi.swipe;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.SystemClock;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class LoginActivity extends AppCompatActivity {

    Button registrati;
    Button accedi;
    String ris;
    EditText txtUser, txtPw;
    public static String idClasse=null;
    JSONArray jsnClassi;
    Context context=this;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        this.context=this;

        registrati = (Button) findViewById(R.id.bt_reg);
        accedi = (Button) findViewById(R.id.bt_login);

        registrati.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(LoginActivity.this,paginaRegistrazione.class);
                startActivity(myIntent);
            }
        });

        accedi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    txtUser = (EditText) findViewById(R.id.InUsername);
                    txtPw = (EditText)findViewById(R.id.InPasswordP);
                    controllo(v);//controlli generali
                    EffettuaLogin lg= new EffettuaLogin(getApplicationContext(),txtUser,txtPw);
                    //EffettuaLogin lg2= new EffettuaLogin(getApplicationContext());
                    lg.execute();
                } catch (Exception e) {
                    e.printStackTrace();

                }
                

            }
        });
    }





    //controlli

    public void controllo(View v)
    {
        EditText EtNome = (EditText) findViewById(R.id.InUsername);
        EditText EtPassword = (EditText)findViewById(R.id.InPasswordP);

        String nome = EtNome.getText().toString();

        String psd = EtPassword .getText().toString(); //Stringa passweord


        TextView errore = (TextView)findViewById(R.id.err_id_login);
        if(nome.length()<1)
        {
            errore.setText("è richiesto un Nome");
            errore.setVisibility(View.VISIBLE);
        }
        else if(psd.length()<1)
        {
            errore.setText("è richiesta una Password");
            errore.setVisibility(View.VISIBLE);
        }
        else if(psd.length()<8){
            errore.setText("Password errata");
            errore.setVisibility(View.VISIBLE);
        }


        else{
            errore.setVisibility(View.INVISIBLE);
            //apertura finestra calendario
        }


    }

}
