
package com.example.alessiotommasi.swipe;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {



    public SectionsPagerAdapter mSectionsPagerAdapter;
    public ViewPager mViewPager;
    public Values valori;
    public TextView mTextMessage;
    public BottomNavigationView navigation;
    public ImageView imgLog,imgCarrello;
    public String isLogged="false";
    public String usr;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        //fragment category
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);


        //navigation bottom
        mTextMessage = (TextView) findViewById(R.id.message);
        navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        valori= new Values();

        //is logged
        try {
            isLogged=getIntent().getExtras().getString("isLogged");
            usr= getIntent().getExtras().getString("user");
            if(isLogged.equals("true")){
                mTextMessage.setText(usr);//perchè non si vede?
            }
            else {
                mTextMessage.setText("not logged");
            }
        }catch (Exception e){//utente non ancora entrato login
            mTextMessage.setText("catch");
            isLogged="false";

            Intent myIntent = new Intent(MainActivity.this,LoginActivity.class);
            startActivity(myIntent);
        }



        //login
        this.imgLog=(ImageView)findViewById(R.id.imgLogin);
        imgLog.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // login activity

                Intent myIntent = new Intent(MainActivity.this,LoginActivity.class);
                startActivity(myIntent);

            }
        });


        //carrello

        this.imgCarrello=(ImageView)findViewById(R.id.imgCarrello);
        imgCarrello.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent myIntent = new Intent(MainActivity.this,Carrello.class);
                myIntent.putExtra("isLogged",isLogged);
                if(isLogged.equals("true")){
                    usr= getIntent().getExtras().getString("user");
                    String id= getIntent().getExtras().getString("id");
                    myIntent.putExtra("UserName",usr);
                    myIntent.putExtra("id",id);

                }

                startActivity(myIntent);

            }
        });
    }





    //navigation bottom
    public BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {

                case R.id.navigation_home:
                    mTextMessage.setText("Home");
                    mViewPager.setCurrentItem(0);
                    return true;
                case R.id.navigation_panini:
                    mTextMessage.setText("Panini");
                    mViewPager.setCurrentItem(1);
                    return true;
                case R.id.navigation_pizze:
                    mTextMessage.setText("Pizza");
                    mViewPager.setCurrentItem(2);
                    return true;
                case R.id.navigation_cotoletta:
                    mTextMessage.setText("Cotoletta");
                    mViewPager.setCurrentItem(3);
                    return true;

                case R.id.navigation_piadine:
                    mTextMessage.setText("Piadine");
                    mViewPager.setCurrentItem(4);
                    return true;


            }
            return false;
        }
    };



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }




}
