package com.example.alessiotommasi.swipe;

import android.content.Context;
import android.os.AsyncTask;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

public class Ordina extends AsyncTask {
    public String str;
    Context context;

    public Ordina(Context context) {
        this.context = context;
    }

    @Override
    protected String doInBackground(Object[] objects) {

        //try get

        String link="http://panelpanini.altervista.org/ordina.php";
        try {//try post

            String data  = URLEncoder.encode("carrello", "UTF-8")+ "=" +
                    URLEncoder.encode(JsonUtil.toJSon(Carrello.dataModels), "UTF-8");

            //adding id
            data += "&" + URLEncoder.encode("id", "UTF-8") + "=" +
                    URLEncoder.encode(Carrello.id, "UTF-8");

            URL url = new URL(link);
            //URLConnection conn = url.openConnection();
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());


            wr.write( data );
            wr.flush();

            InputStream stream = conn.getInputStream();
            //String data = convertStreamToString(stream);
            int sz = stream.available();
            byte[] b = new byte[sz];
            stream.read(b);
            stream.close();
            String data2 = new String(b);

            BufferedReader reader = new BufferedReader(new
                    InputStreamReader(conn.getInputStream()));

            StringBuilder sb = new StringBuilder();
            String line = null;

            // Read Server Response
            while((line = reader.readLine()) != null) {
                sb.append(line);
                break;
            }
            //end db
            str=sb.toString();

        }catch (Exception e){
            e.printStackTrace();
        }
        return str;
    }
    protected void onPreExecute(){
    }



}
