package com.example.alessiotommasi.swipe;

import android.content.ClipData;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public  class PlaceholderFragment extends Fragment {  //action on swipe
    public View rootView;
    private static final String ARG_SECTION_NUMBER = "section_number";
    public BottomNavigationView undNav;
    public MainActivity activity;
    TextView txtTitle;
    LinearLayout hm;
    ScrollView scPanini,scPizza,scCotoletta,scPiadine;
    public ImageView addCarrelloPanini[]=new ImageView[6],addCarrelloPizze[]=new ImageView[2],addCarrelloCotoletta[]=new ImageView[6],addCarrelloPiadine[]=new ImageView[4];
    public SectionsPagerAdapter mSectionsPagerAdapter;
    public ViewPager mViewPager;
    public TextView mTextMessage;

    InternalStorageUtil isu= new InternalStorageUtil();
    public ImageButton btnhPanino,btnhPizza,btnhCotoletta,btnhPiadina;
    int numgerPage;


    //ArrayList <DataModel> paniniCarrello= new ArrayList<DataModel>();

    public PlaceholderFragment() {
    }

    public static PlaceholderFragment newInstance(int sectionNumber) {
        PlaceholderFragment fragment = new PlaceholderFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    //oncreateView
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_main, container, false);
        hm =(LinearLayout)rootView.findViewById(R.id.listHome);
        activity = (MainActivity) getActivity();//questa activty è per prendere obj non del fragment.
        txtTitle = activity.findViewById(R.id.message);
        mTextMessage = (TextView) activity.findViewById(R.id.message);


        //drawer sotto
        undNav=(BottomNavigationView) activity.findViewById(R.id.navigation); //not work se modifico Barzigolo ricorda di chiedere help a riki

        mViewPager = (ViewPager) activity.findViewById(R.id.container);

        numgerPage=getArguments().getInt(ARG_SECTION_NUMBER);
        scPanini= (ScrollView) rootView.findViewById(R.id.scPanini);
        scPizza= (ScrollView) rootView.findViewById(R.id.scPizze);
        scCotoletta= (ScrollView) rootView.findViewById(R.id.scCotolette);
        scPiadine= (ScrollView) rootView.findViewById(R.id.scPiadine);




        //setto pagine via swipe
        setPager(numgerPage);
        //pagine dei bottoni home
        linkBtnHome();
        //ascoltatore delle liste
        listenListElement();
        return rootView;
    }

    //funzione che collega i bottoni della schermata home
    public void linkBtnHome(){
        btnhPanino= (ImageButton) rootView.findViewById(R.id.imgPanino);
        btnhPanino.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //Toast.makeText(getActivity(),"panino cliked ",Toast.LENGTH_SHORT).show();
                mTextMessage.setText("Panini");
                mViewPager.setCurrentItem(1);
            }
        });

        btnhPizza= (ImageButton) rootView.findViewById(R.id.imgPizza);
        btnhPizza.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mTextMessage.setText("Pizza");
                mViewPager.setCurrentItem(2);
            }
        });

        btnhCotoletta= (ImageButton) rootView.findViewById(R.id.imgCotoletta);
        btnhCotoletta.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mTextMessage.setText("Cotoletta");
                mViewPager.setCurrentItem(3);
            }
        });

        btnhPiadina= (ImageButton) rootView.findViewById(R.id.imgPiadina);
        btnhPiadina.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mTextMessage.setText("Piadine");
                mViewPager.setCurrentItem(4);
            }
        });
    }


    public void setPager(int numgerPage){
        switch (numgerPage){
            case 1:
                //undNav.setSelectedItemId(R.id.navigation_home);
                //txtTitle.setText("home");

                scPanini.setVisibility(View.GONE);
                scPizza.setVisibility(View.GONE);
                scCotoletta.setVisibility(View.GONE);
                scPiadine.setVisibility(View.GONE);
                hm.setVisibility(View.VISIBLE);
                //txtInit.setVisibility(View.GONE);
                break;

            case 2://panini
                //undNav.setSelectedItemId(R.id.navigation_panini);

                //txtTitle.setText("panini");
                scPanini.setVisibility(View.VISIBLE);
                scPizza.setVisibility(View.GONE);
                scCotoletta.setVisibility(View.GONE);
                scPiadine.setVisibility(View.GONE);
                hm.setVisibility(View.GONE);


                break;
            case 3://pizze
                //undNav.setSelectedItemId(R.id.navigation_pizze);

                //txtTitle.setText("pizze");
                hm.setVisibility(View.GONE);
                scPanini.setVisibility(View.GONE);
                scPizza.setVisibility(View.VISIBLE);
                scCotoletta.setVisibility(View.GONE);
                scPiadine.setVisibility(View.GONE);


                break;
            case 4://cotoletta
                //undNav.setSelectedItemId(R.id.navigation_cotoletta);

                //txtTitle.setText("cotoletta");
                hm.setVisibility(View.GONE);
                scPanini.setVisibility(View.GONE);
                scPizza.setVisibility(View.GONE);
                scCotoletta.setVisibility(View.VISIBLE);
                scPiadine.setVisibility(View.GONE);


                break;


            case 5://piadine
                //undNav.setSelectedItemId(R.id.navigation_piadine);

                //txtTitle.setText("piadine");
                hm.setVisibility(View.GONE);
                scPanini.setVisibility(View.GONE);
                scPizza.setVisibility(View.GONE);
                scCotoletta.setVisibility(View.GONE);
                scPiadine.setVisibility(View.VISIBLE);
                break;
        }
    }

    //ascoltatore elementi della lista.
    public void listenListElement() {
        try {


            //panini
            TextView tmpTitolo;
            TextView tmpprezzo;
            addCarrelloPanini[0] = (ImageView) rootView.findViewById(R.id.carrellopanino0);//imageView perchè icona +
            addCarrelloPanini[0].setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    // carrello part
                    TextView tmpTitolo = (TextView) rootView.findViewById(R.id.txtTitlePanino0);
                    TextView tmpprezzo = (TextView) rootView.findViewById(R.id.txtPrezzoPanino0);

                    Carrello.dataModels.add(new DataModel(tmpTitolo.getText().toString(), tmpprezzo.getText().toString()));
                    try {
                        //key is filename
                        //isu.writeObject(getContext(), "prova", Carrello.dataModels);
                        Toast.makeText(getActivity(), "" + tmpTitolo.getText() + " aggiunto correttamente", Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }
            });

            addCarrelloPanini[1] = (ImageView) rootView.findViewById(R.id.carrellopanino1);//imageView perchè icona +
            addCarrelloPanini[1].setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    // carrello part
                    TextView tmpTitolo = (TextView) rootView.findViewById(R.id.txtTitlePanino1);
                    TextView tmpprezzo = (TextView) rootView.findViewById(R.id.txtPrezzoPanino1);

                    Carrello.dataModels.add(new DataModel(tmpTitolo.getText().toString(), tmpprezzo.getText().toString()));
                    try {
                        //isu.writeObject(getContext(), "prova", Carrello.dataModels);
                        Toast.makeText(getActivity(), "" + tmpTitolo.getText() + " aggiunto correttamente", Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            addCarrelloPanini[2] = (ImageView) rootView.findViewById(R.id.carrellopanino2);//imageView perchè icona +
            addCarrelloPanini[2].setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    // carrello part
                    TextView tmpTitolo = (TextView) rootView.findViewById(R.id.txtTitlePanino2);
                    TextView tmpprezzo = (TextView) rootView.findViewById(R.id.txtPrezzoPanino2);

                    Carrello.dataModels.add(new DataModel(tmpTitolo.getText().toString(), tmpprezzo.getText().toString()));
                    try {
                        //isu.writeObject(getContext(), "prova", Carrello.dataModels);
                        Toast.makeText(getActivity(), "" + tmpTitolo.getText() + " aggiunto correttamente", Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            addCarrelloPanini[3] = (ImageView) rootView.findViewById(R.id.carrellopanino3);//imageView perchè icona +
            addCarrelloPanini[3].setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    // carrello part
                    TextView tmpTitolo = (TextView) rootView.findViewById(R.id.txtTitlePanino3);
                    TextView tmpprezzo = (TextView) rootView.findViewById(R.id.txtPrezzoPanino3);

                    Carrello.dataModels.add(new DataModel(tmpTitolo.getText().toString(), tmpprezzo.getText().toString()));
                    try {
                        //isu.writeObject(getContext(), "prova", Carrello.dataModels);
                        Toast.makeText(getActivity(), "" + tmpTitolo.getText() + " aggiunto correttamente", Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            addCarrelloPanini[4] = (ImageView) rootView.findViewById(R.id.carrellopanino4);//imageView perchè icona +
            addCarrelloPanini[4].setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    // carrello part
                    TextView tmpTitolo = (TextView) rootView.findViewById(R.id.txtTitlePanino4);
                    TextView tmpprezzo = (TextView) rootView.findViewById(R.id.txtPrezzoPanino4);

                    Carrello.dataModels.add(new DataModel(tmpTitolo.getText().toString(), tmpprezzo.getText().toString()));
                    try {
                        //isu.writeObject(getContext(), "prova", Carrello.dataModels);
                        Toast.makeText(getActivity(), "" + tmpTitolo.getText() + " aggiunto correttamente", Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            addCarrelloPanini[5] = (ImageView) rootView.findViewById(R.id.carrellopanino5);//imageView perchè icona +
            addCarrelloPanini[5].setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    // carrello part
                    TextView tmpTitolo = (TextView) rootView.findViewById(R.id.txtTitlePanino5);
                    TextView tmpprezzo = (TextView) rootView.findViewById(R.id.txtPrezzoPanino5);

                    Carrello.dataModels.add(new DataModel(tmpTitolo.getText().toString(), tmpprezzo.getText().toString()));
                    try {
                        //isu.writeObject(getContext(), "prova", Carrello.dataModels);
                        Toast.makeText(getActivity(), "" + tmpTitolo.getText() + " aggiunto correttamente", Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            //endPanini

            //pizze

            addCarrelloPizze[0] = (ImageView) rootView.findViewById(R.id.carrelloPizza0);//imageView perchè icona +
            addCarrelloPizze[0].setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    // carrello part
                    TextView tmpTitolo = (TextView) rootView.findViewById(R.id.txtTitlePizza0);
                    TextView tmpprezzo = (TextView) rootView.findViewById(R.id.txtPrezzoPanino0);

                    Carrello.dataModels.add(new DataModel(tmpTitolo.getText().toString(), tmpprezzo.getText().toString()));
                    try {
                        //isu.writeObject(getContext(), "prova", Carrello.dataModels);
                        Toast.makeText(getActivity(), "" + tmpTitolo.getText() + " aggiunto correttamente", Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            addCarrelloPizze[1] = (ImageView) rootView.findViewById(R.id.carrelloPizza1);//imageView perchè icona +
            addCarrelloPizze[1].setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    TextView tmpTitolo = (TextView) rootView.findViewById(R.id.txtTitlePizza1);
                    TextView tmpprezzo = (TextView) rootView.findViewById(R.id.txtPrezzoPanino1);

                    Carrello.dataModels.add(new DataModel(tmpTitolo.getText().toString(), tmpprezzo.getText().toString()));
                    try {
                        //isu.writeObject(getContext(), "prova", Carrello.dataModels);
                        Toast.makeText(getActivity(), "" + tmpTitolo.getText() + " aggiunto correttamente", Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            //endPizze

            //cotoletta

            addCarrelloCotoletta[0] = (ImageView) rootView.findViewById(R.id.carrelloCotoletta0);//imageView perchè icona +
            addCarrelloCotoletta[0].setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    TextView tmpTitolo = (TextView) rootView.findViewById(R.id.txtTitleCotoletta0);
                    TextView tmpprezzo = (TextView) rootView.findViewById(R.id.txtPrezzoCotoletta0);

                    Carrello.dataModels.add(new DataModel(tmpTitolo.getText().toString(), tmpprezzo.getText().toString()));
                    try {
                        //isu.writeObject(getContext(), "prova", Carrello.dataModels);
                        Toast.makeText(getActivity(), "" + tmpTitolo.getText() + " aggiunto correttamente", Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            addCarrelloCotoletta[1] = (ImageView) rootView.findViewById(R.id.carrelloCotoletta1);//imageView perchè icona +
            addCarrelloCotoletta[1].setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    TextView tmpTitolo = (TextView) rootView.findViewById(R.id.txtTitleCotoletta1);
                    TextView tmpprezzo = (TextView) rootView.findViewById(R.id.txtPrezzoCotoletta1);

                    Carrello.dataModels.add(new DataModel(tmpTitolo.getText().toString(), tmpprezzo.getText().toString()));
                    try {
                        //isu.writeObject(getContext(), "prova", Carrello.dataModels);
                        Toast.makeText(getActivity(), "" + tmpTitolo.getText() + " aggiunto correttamente", Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            addCarrelloCotoletta[2] = (ImageView) rootView.findViewById(R.id.carrelloCotoletta2);//imageView perchè icona +
            addCarrelloCotoletta[2].setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    TextView tmpTitolo = (TextView) rootView.findViewById(R.id.txtTitleCotoletta2);
                    TextView tmpprezzo = (TextView) rootView.findViewById(R.id.txtPrezzoCotoletta2);

                    Carrello.dataModels.add(new DataModel(tmpTitolo.getText().toString(), tmpprezzo.getText().toString()));
                    try {
                        //isu.writeObject(getContext(), "prova", Carrello.dataModels);
                        Toast.makeText(getActivity(), "" + tmpTitolo.getText() + " aggiunto correttamente", Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            addCarrelloCotoletta[3] = (ImageView) rootView.findViewById(R.id.carrelloCotoletta3);//imageView perchè icona +
            addCarrelloCotoletta[3].setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    TextView tmpTitolo = (TextView) rootView.findViewById(R.id.txtTitleCotoletta3);
                    TextView tmpprezzo = (TextView) rootView.findViewById(R.id.txtPrezzoCotoletta3);

                    Carrello.dataModels.add(new DataModel(tmpTitolo.getText().toString(), tmpprezzo.getText().toString()));
                    try {
                        //isu.writeObject(getContext(), "prova", Carrello.dataModels);
                        Toast.makeText(getActivity(), "" + tmpTitolo.getText() + " aggiunto correttamente", Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            addCarrelloCotoletta[4] = (ImageView) rootView.findViewById(R.id.carrelloCotoletta4);//imageView perchè icona +
            addCarrelloCotoletta[4].setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    TextView tmpTitolo = (TextView) rootView.findViewById(R.id.txtTitleCotoletta4);
                    TextView tmpprezzo = (TextView) rootView.findViewById(R.id.txtPrezzoCotoletta4);

                    Carrello.dataModels.add(new DataModel(tmpTitolo.getText().toString(), tmpprezzo.getText().toString()));
                    try {
                        //isu.writeObject(getContext(), "prova", Carrello.dataModels);
                        Toast.makeText(getActivity(), "" + tmpTitolo.getText() + " aggiunto correttamente", Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            addCarrelloCotoletta[5] = (ImageView) rootView.findViewById(R.id.carrelloCotoletta5);//imageView perchè icona +
            addCarrelloCotoletta[5].setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    TextView tmpTitolo = (TextView) rootView.findViewById(R.id.txtTitleCotoletta5);
                    TextView tmpprezzo = (TextView) rootView.findViewById(R.id.txtPrezzoCotoletta5);

                    Carrello.dataModels.add(new DataModel(tmpTitolo.getText().toString(), tmpprezzo.getText().toString()));
                    try {
                        //isu.writeObject(getContext(), "prova", Carrello.dataModels);
                        Toast.makeText(getActivity(), "" + tmpTitolo.getText() + " aggiunto correttamente", Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            //endCotoletta

            //Piadine

            addCarrelloPiadine[0] = (ImageView) rootView.findViewById(R.id.carrelloPiadina0);//imageView perchè icona +
            addCarrelloPiadine[0].setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    TextView tmpTitolo = (TextView) rootView.findViewById(R.id.txtTitlePiadina0);
                    TextView tmpprezzo = (TextView) rootView.findViewById(R.id.txtPrezzoPiadina0);

                    Carrello.dataModels.add(new DataModel(tmpTitolo.getText().toString(), tmpprezzo.getText().toString()));
                    try {
                        //isu.writeObject(getContext(), "prova", Carrello.dataModels);
                        Toast.makeText(getActivity(), "" + tmpTitolo.getText() + " aggiunto correttamente", Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            addCarrelloPiadine[1] = (ImageView) rootView.findViewById(R.id.carrelloPiadina1);//imageView perchè icona +
            addCarrelloPiadine[1].setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    TextView tmpTitolo = (TextView) rootView.findViewById(R.id.txtTitlePiadina1);
                    TextView tmpprezzo = (TextView) rootView.findViewById(R.id.txtPrezzoPiadina1);

                    Carrello.dataModels.add(new DataModel(tmpTitolo.getText().toString(), tmpprezzo.getText().toString()));
                    try {
                        //isu.writeObject(getContext(), "prova", Carrello.dataModels);
                        Toast.makeText(getActivity(), "" + tmpTitolo.getText() + " aggiunto correttamente", Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            addCarrelloPiadine[2] = (ImageView) rootView.findViewById(R.id.carrelloPiadina2);//imageView perchè icona +
            addCarrelloPiadine[2].setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    TextView tmpTitolo = (TextView) rootView.findViewById(R.id.txtTitlePiadina2);
                    TextView tmpprezzo = (TextView) rootView.findViewById(R.id.txtPrezzoPiadina2);

                    Carrello.dataModels.add(new DataModel(tmpTitolo.getText().toString(), tmpprezzo.getText().toString()));
                    try {
                        //isu.writeObject(getContext(), "prova", Carrello.dataModels);
                        Toast.makeText(getActivity(), "" + tmpTitolo.getText() + " aggiunto correttamente", Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            addCarrelloPiadine[3] = (ImageView) rootView.findViewById(R.id.carrelloPiadina3);//imageView perchè icona +
            addCarrelloPiadine[3].setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    TextView tmpTitolo = (TextView) rootView.findViewById(R.id.txtTitlePiadina3);
                    TextView tmpprezzo = (TextView) rootView.findViewById(R.id.txtPrezzoPiadina3);

                    Carrello.dataModels.add(new DataModel(tmpTitolo.getText().toString(), tmpprezzo.getText().toString()));
                    try {
                        //isu.writeObject(getContext(), "prova", Carrello.dataModels);
                        Toast.makeText(getActivity(), "" + tmpTitolo.getText() + " aggiunto correttamente", Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            //EndPiadine
        }catch (Exception e){e.printStackTrace();}
        }
}
