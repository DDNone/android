package com.example.alessiotommasi.swipe;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class paginaRegistrazione extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pagina_registrazione);
    }
    public boolean numero( String s )
    {
        Pattern p = Pattern.compile( "[0-9]" );
        Matcher m = p.matcher( s );

        return m.find();
    }
    public boolean controlloNomeUtente ( String s )
    {
        // query
        //if ( controllo della query)
        return true;
        //else
        //   return false;
    }
    public void finishEdit(View v)
    {
        EditText EtNome = (EditText) findViewById(R.id.InNome);
        EditText EtCognome = (EditText) findViewById(R.id.InCognome);
        EditText EtNomeUtente = (EditText) findViewById(R.id.InNomeUtente);
        EditText EtPassword = (EditText)findViewById(R.id.InPassword);
        EditText EtConferma = (EditText)findViewById(R.id.InConfermaPassword);

        String nome = EtNome.getText().toString();
        String cognome = EtCognome.getText().toString();
        String nomeUtente = EtNomeUtente.getText().toString();
        String psd = EtPassword .getText().toString(); //Stringa passweord
        String conferma = EtConferma.getText().toString();   //Stringa conferma passweord

        TextView errore = (TextView)findViewById(R.id.err_pwd);
        if(nome.length()<1)
        {
            errore.setText("è richiesto un Nome");
            errore.setVisibility(View.VISIBLE);
        }
        else if(cognome.length()<1)
        {
            errore.setText("è richiesto un Cognome");
            errore.setVisibility(View.VISIBLE);
        }
        else if(nomeUtente.length()<1)
        {
            errore.setText("è richiesto un Nome Utente");
            errore.setVisibility(View.VISIBLE);
        }
        else if(!controlloNomeUtente(nomeUtente))
        {
            errore.setText("Nome utente già in uso");
            errore.setVisibility(View.VISIBLE);
        }
        else if(!numero(psd))
        {
            errore.setText("è richiesto almeno un numero nella password");
            errore.setVisibility(View.VISIBLE);
        }
        else if(psd.length()<1)
        {
            errore.setText("è richiesta una Password");
            errore.setVisibility(View.VISIBLE);
        }
        else if(psd.length()<8)
        {
            errore.setText("password troppo corta(min 8 caratt.)");
            errore.setVisibility(View.VISIBLE);
        }
        else if(conferma.length()<1)
        {
            errore.setText("è richiesta la conferma della Password");
            errore.setVisibility(View.VISIBLE);
        }
        else if(!psd.equals(conferma))
        {
            errore.setText("errore password non uguale");
            errore.setVisibility(View.VISIBLE);
        }
        else{
            errore.setVisibility(View.INVISIBLE);
            //SALVATAGGIO DATI SU DB
            String password=MD5.crypt(psd);
            //apertura finestra calendario
            //controlli superati
            Intent myIntent = new Intent(paginaRegistrazione.this,LoginActivity.class);
            startActivity(myIntent);
        }

    }


}
